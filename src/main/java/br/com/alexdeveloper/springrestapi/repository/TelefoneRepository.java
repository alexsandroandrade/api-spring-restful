package br.com.alexdeveloper.springrestapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.alexdeveloper.springrestapi.model.Telefone;

@Repository
public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
	
	List<Telefone> findByNumeroContains(String numTelefone);


}
