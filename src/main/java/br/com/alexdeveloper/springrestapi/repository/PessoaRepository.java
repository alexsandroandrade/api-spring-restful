package br.com.alexdeveloper.springrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alexdeveloper.springrestapi.model.Pessoa;



public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

}
