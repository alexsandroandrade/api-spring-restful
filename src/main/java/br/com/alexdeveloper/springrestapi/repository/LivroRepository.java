package br.com.alexdeveloper.springrestapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alexdeveloper.springrestapi.model.Livro;

public interface LivroRepository extends JpaRepository<Livro, Long>{
	
	
	
	List<Livro> findByTituloContains(String titulo);


}
