package br.com.alexdeveloper.springrestapi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alexdeveloper.springrestapi.model.Usuario;
import br.com.alexdeveloper.springrestapi.repository.UsuarioRepository;
import br.com.alexdeveloper.springrestapi.service.TelefoneService;
import br.com.alexdeveloper.springrestapi.util.ImplementacaoUserDetailsService;
import br.com.alexdeveloper.springrestapi.util.ServiceRelatorio;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/usuario")
public class IndexController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private ImplementacaoUserDetailsService implementacaoUserDetailsService;
	
	
	@Autowired
	private ServiceRelatorio serviceRelatorio;

	@GetMapping(value = "/{id}", produces = "application/json")
	@CachePut("cacheuser")
	public ResponseEntity<Usuario> buscarPorId(@PathVariable(value = "id") Long id) {
		Usuario usuario = usuarioRepository.findById(id).get();
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
	
	@GetMapping(value = "/", produces = "application/json")
	@CachePut("cacheusuarios")
	public ResponseEntity<List<Usuario>> buscarTodos() {
		List<Usuario> list = usuarioRepository.findAll();
		return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
	}
	
	@PostMapping(value = "/", produces = "application/json")
	public ResponseEntity<Usuario> cadastrar(@RequestBody Usuario usuario) {

		// Caso o telefone não seja salvo automaticamente
//		for (int pos = 0; pos < usuario.getTelefones().size(); pos++) {
//			usuario.getTelefones().get(pos).setUsuario(usuario);
//		}

		String senhaCriptografada = new BCryptPasswordEncoder().encode(usuario.getSenha());
		usuario.setSenha(senhaCriptografada);
		Usuario usuarioSalvo = usuarioRepository.save(usuario);

		implementacaoUserDetailsService.inserirAcessoPadrao(usuario.getId());

		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
	}
	
	@PutMapping(value = "/", produces = "application/json")
	public ResponseEntity<Usuario> atualizar(@RequestBody Usuario usuario) {

//		// Caso o telefone não seja salvo automaticamente
//		for (int pos = 0; pos < usuario.getTelefones().size(); pos++) {
//			usuario.getTelefones().get(pos).setUsuario(usuario);
//		}

		Usuario userTemp = usuarioRepository.findById(usuario.getId()).get();

		if (!userTemp.getSenha().equals(usuario.getSenha())) {
			String senhaCriptografada = new BCryptPasswordEncoder().encode(usuario.getSenha());
			usuario.setSenha(senhaCriptografada);
		}

		Usuario usuarioSalvo = usuarioRepository.save(usuario);
		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
	}
	
	@GetMapping(value = "/usuarioPorLogin/{login}", produces = "application/json")
	@CachePut("cacheusuarios")
	public ResponseEntity<List<Usuario>> buscarUsuarioPorLogin(@PathVariable("login") String login) {	
		
	
		
		List<Usuario> usuariosList = usuarioRepository.findByLoginContains(login);
		
		return new ResponseEntity<List<Usuario>>(usuariosList, HttpStatus.OK);
	}
		
	@DeleteMapping(value = "/{id}", produces = "application/text")
	public String delete(@PathVariable("id") Long id) {
		usuarioRepository.deleteById(id);
		return "ok";
	}
	
	@GetMapping(value="/relatorio", produces = "application/text")
	public ResponseEntity<String> downloadRelatorio(HttpServletRequest request) throws Exception{
		
		byte[] pdf = serviceRelatorio.gerarRelatorio("relatorio-usuario", request.getServletContext());
		
		String base64pdf = "data:application/pdf;base64," +  Base64.encodeBase64String(pdf);
		
		return new ResponseEntity<String>(base64pdf, HttpStatus.OK);
	}
	
	

}
