package br.com.alexdeveloper.springrestapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alexdeveloper.springrestapi.model.Livro;
import br.com.alexdeveloper.springrestapi.service.LivroService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/livro")
public class LivroController {

	@Autowired
	private LivroService livroService;

	@GetMapping(value = "/{id}", produces = "application/json")
	@CachePut("cacheuser")
	public ResponseEntity<Livro> buscarPorId(@PathVariable(value = "id") Long id) {
		Livro livro = livroService.findById(id).get();
		return new ResponseEntity<Livro>(livro, HttpStatus.OK);
	}

	@GetMapping(value = "/", produces = "application/json")
	@CachePut("cachelivros")
	public ResponseEntity<List<Livro>> buscarTodos() {
		List<Livro> list = livroService.findAll();
		return new ResponseEntity<List<Livro>>(list, HttpStatus.OK);
	}
	
	@PostMapping(value = "/", produces = "application/json")
	public ResponseEntity<Livro> cadastrar(@RequestBody Livro livro) {
		
		Livro livroSalvo = livroService.save(livro);

		return new ResponseEntity<Livro>(livroSalvo, HttpStatus.OK);
	}
	
	@PutMapping(value = "/", produces = "application/json")
	public ResponseEntity<Livro> atualizar(@RequestBody Livro livro) {

		
		Livro userTemp = livroService.findById(livro.getId()).get();		

		Livro livroSalvo = livroService.save(livro);
		return new ResponseEntity<Livro>(livroSalvo, HttpStatus.OK);
	}

	@GetMapping(value = "/livroPorTitulo/{titulo}", produces = "application/json")
	@CachePut("cachelivros")
	public ResponseEntity<List<Livro>> buscarLivroPorTitulo(@PathVariable("titulo") String titulo) {

		List<Livro> livrosList = livroService.findByTituloContains(titulo);

		return new ResponseEntity<List<Livro>>(livrosList, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = "application/text")
	public String delete(@PathVariable("id") Long id) {
		
		Livro userTemp = livroService.findById(id).get();		

		livroService.delete(userTemp);
		return "ok";
	}

}
