package br.com.alexdeveloper.springrestapi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.alexdeveloper.springrestapi.model.Pessoa;
import br.com.alexdeveloper.springrestapi.model.Telefone;
import br.com.alexdeveloper.springrestapi.service.PessoaService;
import br.com.alexdeveloper.springrestapi.service.TelefoneService;


@RestController
@RequestMapping("/pessoa")
public class PessoaController {

	@Autowired
	private PessoaService pessoaService;
	

	@Autowired
	private TelefoneService telefoneService;	
	
	@JsonIgnore
	@GetMapping("/{id}")
	public ResponseEntity<Pessoa> findById(@PathVariable Long id) {
		return pessoaService.findById(id).map(pessoa -> ResponseEntity.ok(pessoa)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping
	public List<Pessoa> findAll() {
		return pessoaService.findAll();
	}
	
	@PutMapping("/")
	public ResponseEntity<Pessoa> update(@RequestBody Pessoa pessoa){

		Pessoa pessoaSalvo = pessoaService.save(pessoa);
		return new ResponseEntity<Pessoa>(pessoaSalvo, HttpStatus.OK);
	}

	@PostMapping
	public Pessoa cadastrar(@RequestBody Pessoa pessoa) {
		
// 		Caso o telefone não seja salvo automaticamente
		for (int pos = 0; pos < pessoa.getTelefones().size(); pos++) {
			pessoa.getTelefones().get(pos).setPessoa(pessoa);
		}
		return pessoaService.save(pessoa);
	}
	
	@DeleteMapping("/{id}") 
	public ResponseEntity<Object> delete(@PathVariable Long id){
		return pessoaService.findById(id).map(pessoa -> {
			pessoaService.delete(pessoa);
			return ResponseEntity.noContent().build(); 
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@DeleteMapping(value = "/removerTelefone/{id}", produces = "application/text")
	public String deleteTelefone(@PathVariable("id") Long id) {
		telefoneService.delete(id);
		return "ok";
	}

}
