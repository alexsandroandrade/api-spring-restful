package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alexdeveloper.springrestapi.model.Telefone;
import br.com.alexdeveloper.springrestapi.repository.TelefoneRepository;

@Service
public class TelefoneServiceImpl implements TelefoneService{
	
	@Autowired
	private TelefoneRepository telefoneRepository;

	@Override
	public Optional<Telefone> findById(Long id) {
		return telefoneRepository.findById(id);
	}

	@Override
	public List<Telefone> findAll() {
		return telefoneRepository.findAll();
	}

	@Override
	public Telefone save(Telefone livro) {
		return telefoneRepository.save(livro);
	}

	@Override
	public void delete(Long id) {
		telefoneRepository.deleteById(id);
	}

	@Override
	public List<Telefone> findByNumeroContains(String numTelefone) {
		return telefoneRepository.findByNumeroContains(numTelefone);
	}

}
