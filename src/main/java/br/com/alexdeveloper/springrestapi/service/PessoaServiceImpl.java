package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alexdeveloper.springrestapi.model.Pessoa;
import br.com.alexdeveloper.springrestapi.repository.PessoaRepository;


@Service
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	PessoaRepository pessoaRepository;
	
	
	@Override
	public Optional<Pessoa> findById(Long id) {
		return pessoaRepository.findById(id);
	}

	@Override
	public List<Pessoa> findAll() {
		return pessoaRepository.findAll();
	}

	@Override
	public Pessoa save(Pessoa usuario) {
		return pessoaRepository.save(usuario);
	}

	@Override
	public void delete(Pessoa usuario) {
		pessoaRepository.delete(usuario);
	}

}
