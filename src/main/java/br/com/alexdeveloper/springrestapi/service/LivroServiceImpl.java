package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alexdeveloper.springrestapi.model.Livro;
import br.com.alexdeveloper.springrestapi.repository.LivroRepository;

@Service
public class LivroServiceImpl implements LivroService {
	
	@Autowired
	LivroRepository livroRepository;

	@Override
	public Optional<Livro> findById(Long id) {
		// TODO Auto-generated method stub
		return livroRepository.findById(id);
	}

	@Override
	public List<Livro> findAll() {
		// TODO Auto-generated method stub
		return livroRepository.findAll();
	}

	@Override
	public Livro save(Livro livro) {
		// TODO Auto-generated method stub
		return livroRepository.save(livro);
	}

	@Override
	public void delete(Livro livro) {
		// TODO Auto-generated method stub
		livroRepository.delete(livro);
		
	}

	@Override
	public List<Livro> findByTituloContains(String titulo) {
		// TODO Auto-generated method stub
		return livroRepository.findByTituloContains(titulo);
	}

}
