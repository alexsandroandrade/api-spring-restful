package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import br.com.alexdeveloper.springrestapi.model.Telefone;


public interface TelefoneService {
	
	Optional<Telefone> findById(Long id);

	List<Telefone> findAll();

	Telefone save(Telefone telefone);

	void delete(Long id);
	
	List<Telefone> findByNumeroContains(String numTelefone);

}
