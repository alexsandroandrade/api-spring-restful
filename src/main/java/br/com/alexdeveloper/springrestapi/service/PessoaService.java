package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import br.com.alexdeveloper.springrestapi.model.Pessoa;


public interface PessoaService {

	Optional<Pessoa> findById(Long id);
	
	List<Pessoa> findAll();
	
	Pessoa save(Pessoa usuario);

	void delete(Pessoa usuario);


}
