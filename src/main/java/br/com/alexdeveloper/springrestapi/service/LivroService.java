package br.com.alexdeveloper.springrestapi.service;

import java.util.List;
import java.util.Optional;

import br.com.alexdeveloper.springrestapi.model.Livro;

public interface LivroService {

	Optional<Livro> findById(Long id);

	List<Livro> findAll();

	Livro save(Livro livro);

	void delete(Livro livro);
	
	List<Livro> findByTituloContains(String titulo);


}
