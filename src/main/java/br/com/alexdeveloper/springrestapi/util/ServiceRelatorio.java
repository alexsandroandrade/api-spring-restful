package br.com.alexdeveloper.springrestapi.util;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import br.com.alexdeveloper.springrestapi.model.Usuario;
import br.com.alexdeveloper.springrestapi.repository.UsuarioRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ServiceRelatorio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private transient JdbcTemplate jdbcTemplate;

	@Autowired
	private UsuarioRepository usuarioRepository;

//	final JasperReport report = loadTemplate("path arquivo");
//	JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(listDoObjetos);
//	final Map<String, Object> parameters = parameters(parametros);
//	JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, data);
//	bytes = JasperExportManager.exportReportToPdf(jasperPrint);

	public byte[] gerarRelatorio(String nomeRelatorio, ServletContext servletContext) throws JRException, SQLException {

		// Obter conexão com o banco de dados
		try (Connection connection = jdbcTemplate.getDataSource().getConnection()) {

			// Carregar o caminho do arquivo Jasper
			String caminhoJasper = servletContext.getRealPath("relatorios") + File.separator + nomeRelatorio
					+ ".jasper";

			List<Usuario> usuariosReportList = usuarioRepository.findAll();
			
			JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(usuariosReportList);


			// Gerar relatório com os dados e conexão
			JasperPrint print = JasperFillManager.fillReport(caminhoJasper, new HashMap<>(), data);
			


			// Exporta para byte o PDF para fazer o download
			return JasperExportManager.exportReportToPdf(print);
		}
	}

}
